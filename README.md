# gofileserver

This is an easy file server

### Usage

* `-help`

    Show help message;

* `-port`

    Set listen port, if not set, 31100 will be used;

* `-root`

    Set the directoy show as root, if not set, `./` will be used;     